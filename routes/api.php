<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('managers', 'Api\ManagerController@store')->name('managers.store');
Route::get('managers', 'Api\ManagerController@index')->name('managers.index');

Route::post('manager-rooms', 'Api\ManagerController@assignRooms')->name('managers.assign.rooms');
Route::get('manager-rooms', 'Api\ManagerController@getManagersWithRooms')->name('managers.rooms');

Route::post('rooms', 'Api\RoomController@store')->name('rooms.store');
Route::get('rooms', 'Api\RoomController@index')->name('rooms.index');

Route::group(['middleware' => 'auth:api'], function(){
    Route::post('bookings', 'Api\BookingController@store')->name('bookings.store');
    // Route::get('bookings', 'Api\BookingController@index')->name('bookings.index');
});
