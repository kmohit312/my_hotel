# My hotel

This is a dummy laravel API project.
It is having following functionalities.

Frontend:
1. Customer can register/login from frontend.

API:
1. Add manager
2. Add room
3. Managers list
4. Manager assign rooms
5. Managers list with rooms
6. Generate auth token to get auth token of customer
6. Add room booking by customer using auth token
7. Rooms list with bookings