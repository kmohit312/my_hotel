<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManagerRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manager_rooms', function (Blueprint $table) {
            // $table->bigIncrements('id');
            $table->integer('manager_id');
            $table->integer('room_id');
            // $table->timestamps();
        });

        DB::unprepared('ALTER TABLE `manager_rooms` ADD PRIMARY KEY (`manager_id`,`room_id`)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manager_rooms');
    }
}
