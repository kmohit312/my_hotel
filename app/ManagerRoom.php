<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManagerRoom extends Model
{
	public $timestamps = false;
	
    protected $fillable = [
        'manager_id', 'room_id'
    ];

    protected $hidden = [
        'created_at','updated_at'
    ];

    public function room(){
    	return $this->belongsTo('App\Room');
    }

    public function manager(){
    	return $this->belongsTo('App\User','manager_id','id');
    }

}
