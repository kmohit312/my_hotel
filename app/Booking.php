<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillable = [
        'user_id', 'room_id', 'date'
    ];

    public function room(){
    	return $this->belongsTo('App\Room');
    }

    public function user(){
    	return $this->belongsTo('App\User','user_id','id');
    }

}
