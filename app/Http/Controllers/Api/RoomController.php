<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Room;
use Validator;

class RoomController extends Controller
{
    public function index(Request $request){
        
        $rooms = Room::with(['bookings.user','bookings.room'])->get();
        
        if(!$rooms->isEmpty()) {
            return response()->json([
                'message' => 'Rooms list',
                'data' => $rooms
            ], 200);
        } else{
            return response()->json([
                'message' => 'Rooms list is empty',
            ], 404);
        }    
    }

    public function store(Request $request){
    	$validator = Validator::make($request->all(), [
    			'room_no' => 'required|unique:'.(new Room)->getTable().',room_no',
    			'floor_no' => 'required|numeric',
    			'price' => 'required'
    		]);

    	if($validator->fails()){
    		return response()->json([ 'message' => $validator->errors()->first() ], 400 );
    	} else{

            $room = Room::create($request->input());
            if(!empty($room)){
                return response()->json([
                    'message' => 'Room has been added successfully',
                    'data' => $room
                ], 200);
            } else{
                return response()->json([
                    'message' => Config('constant.common_err')
                ], 400);
            }
    	}
    }

}
