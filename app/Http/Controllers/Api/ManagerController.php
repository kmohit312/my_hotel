<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Validation\Rule;
use Hash;

use App\User;
use App\ManagerRoom;

class ManagerController extends Controller
{
    public function index(Request $request){
        
        $users = User::select('id','name','email','phone')->isManager()->get();
        
        if(!$users->isEmpty()) {
            return response()->json([
                'message' => 'Managers list',
                'data' => $users
            ], 200);
        } else{
            return response()->json([
                'message' => 'Managers list is empty',
            ], 404);
        }    
    }

    public function store(Request $request){
    	$validator = Validator::make($request->all(), [
    			'name' => 'required|max:100',
    			'email' => 'required|email|unique:'.(new User)->getTable().',email|max:100',
    			'phone' => 'required|max:15',
    			'password' => 'required|min:8|max:100|confirmed',
    		]);

    	if($validator->fails()){
    		return response()->json([ 'message' => $validator->errors()->first() ], 400 );
    	} else{

            $request->merge(['role' => 'M']);
            $request->merge(['password' => Hash::make($request->password)]);

            $user = User::create($request->all());
            if(!empty($user)){
                return response()->json([
                    'message' => 'Manager has been added successfully',
                    'data' => $user
                ], 200);
            } else{
                return response()->json([
                    'message' => Config('constant.common_err')
                ], 400);
            }
    	}
    }

    public function assignRooms(Request $request){
        $validator = Validator::make($request->all(), [
                'manager_id' => [
                    'required',
                    Rule::exists((new User)->getTable(),'id')->where(function($query){
                        $query->where('role','M');
                    })
                ],
                'room_ids' => 'required|array'
            ]);

        if($validator->fails()){
            return response()->json([ 'message' => $validator->errors()->first() ], 400 );
        } else{

            $user = ManagerRoom::whereManagerId($request->manager_id)->delete();

            $room_ids = array_unique($request->room_ids);
            $manager_rooms = [];

            foreach($room_ids as $key => $room_id){
                $manager_rooms[$key]['manager_id']  = $request->manager_id;
                $manager_rooms[$key]['room_id']     = $room_id;
            }

            if(ManagerRoom::insert($manager_rooms)) {
                return response()->json([
                    'message' => 'Room has been assigned to manager successfully',
                ], 200);
            } else{
                return response()->json([
                    'message' => Config('constant.common_err')
                ], 400);
            }
        }

    }

    public function getManagersWithRooms(Request $request){
        
        $users = User::select('id','name','email','phone')
                ->isManager()
                ->with('assignedRooms.room')
                ->get();
        
        if(!$users->isEmpty()) {
            return response()->json([
                'message' => 'Managers list',
                'data' => $users
            ], 200);
        } else{
            return response()->json([
                'message' => 'Managers list is empty',
                // 'data' => $users
            ], 404);
        }    
    }

}
