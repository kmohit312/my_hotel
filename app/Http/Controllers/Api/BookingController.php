<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Room;
use App\Booking;
use Validator;
use Auth;

class BookingController extends Controller
{
    public function store(Request $request){
    	$validator = Validator::make($request->all(), [
    			'room_id' => 'required|exists:'.(new Room)->getTable().',id',
    			'date' => 'required|date_format:Y-m-d',
    		]);

    	if($validator->fails()){
    		return response()->json([ 'message' => $validator->errors()->first() ], 400 );
    	} else{

            $request->merge(['user_id' => Auth::user()->id ]);
            $user = Booking::create($request->all());
            if(!empty($user)){
                return response()->json([
                    'message' => 'Booking has been added successfully',
                    'data' => $user
                ], 200);
            } else{
                return response()->json([
                    'message' => Config('constant.common_err')
                ], 400);
            }
    	}
    }

}
