<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = [
        'room_no', 'floor_no', 'price'
    ];

    protected $hidden = [
        'created_at','updated_at'
    ];

    public function bookings(){
    	return $this->hasMany('App\Booking');
    }

}
